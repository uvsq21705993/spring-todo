FROM java:8
EXPOSE 8080
ADD /target/docker-spring-boot.jar docker-spring-boot.jar
ENTRYPOINT ["java","-jar","docker-spring-boot.jar","com.example.demo.SrpingTodoApplicationTests"]

