package com.example.demo;

import java.util.ArrayList;

public class Tache {
	private String nomTache;
	private String descriptionTache;
	
	public Tache(String nomTache, String descriptionTache) {
		this.nomTache= nomTache;
		this.descriptionTache=descriptionTache;
		
	}
	
	public String  getNomT() {
		return nomTache;
	}
	
	public String getDesT() {
		return descriptionTache;
	}
	
	public String toString() {
		return "Tache : " + this.nomTache+ " faire: "+ this.descriptionTache;
	}

}

class ListeTache{
	private ArrayList<Tache> taches= new ArrayList<Tache>();
	public String toString() {
		String Newligne=System.getProperty("line.separator"); 
		String s="";
		for(Tache t : taches) {
			s=s+ t.toString()+ Newligne +"\t";
			
			
		}
		return s;
		
	}
}
